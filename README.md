# NEW INPUT MAP FOR GODOT 3.1
## Version 0.9
## © Xecestel
## Licensed Under MPL 2.0 (see below)

This plugin allows you to create a custom mapping for your game`s controls without using the InputMap settings. Why? Because this plugin comes with new useful features, like better modifiers support, easier configuration (using only Strings) and a save and load data feature!


## How does it work?
This script replaces Input and InputEvent methods to check the input. Basically it`s a whole new input manager for your project.
If you want to use the classic InputMap, this ControlMap script or both, do it at your discretion. Be aware, though that if the default ui actions exist both in this script and in the ProjectSettings InputMap, weird things could happen. 


## Configuration
First things first: remember that to use this plugin you have to set it as a Singleton in the ProjectSettings > AutoLoad tab. The only script that is required as an AutoLoad is the ControlMap.gd file.

In order to work, this plugin uses 6 dictionaries: 2 variable and 4 constant. The 4 constant dictionaries are contained in the ConstantDictionaries.gd file and are used to make every button and key codes to an alphabetic string for easier usage of the script. You don't need to touch them (and I don't advise you to if you don't know what you're doing), but you can check them if you want to know what names you can use for the buttons.
What do you need them for?
To populate the mapping dictionaries.
There's two of them, both contained in the NewInputMapTemplate.gd file: one for the keyboard and mouse and one for the joypad. Feel free to add and remove all the actions you want, just be sure to make every one of them correspond to at least a key or button and that this key or button exists in the other dictionaries. Use the example template that comes in the script.
The values in the Dictionaries are not just strings: they are Arrays of Array of Strings. This means that you can also add multiple buttons per action and a modifier for every button.
So, let's assume you want to make the player go left with the Left key or by pressing Shift and D, for some reason. To do that you will have to make an entry in the KeyBoardMap Dictionary that looks like this:
```
"left"	:	[ ["Left"], ["Shift, "D"] ]
```

## Methods
Now, here`s a list of all the methods you can use in your code and how they work:
- `ControlMap.is_action_pressed(action)`:
This method replaces `Input.is_action_pressed method`. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being pressed.
- `ControlMap.is_action_just_pressed(action)`:
This method replaces `Input.is_action_just_pressed` method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being just pressed.
- `ControlMap.is_event_pressed(action, event)`:
This method works with the `_input(event)` method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). `event` is an `InputEvent` and it's basically the argument of the `_input(event)` method. This method returns a boolean value that tells you if the event pressed corresponds to the given action.
- `ControlMap.is_event_just_pressed(action, event)`:
This method works with the `_input(event)` method. Actions is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). Event is an InputEvent and it's basically the argument of the `_input(event) method.`
This method it's similar to the `ControlMap.is_event_pressed` method but with a `just_pressed` logic added.
- `ControlMap.is_action_released(action)`:
This method replaces Input.is_action_just_released method. Action is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being just released.


To use this script along with a controls remapping scene, you will have to familiarize with the following methods:
- `ControlMap.set_keyboard_button_name(action, button_name, modifier_name = "", control_index = 0)`:
This method lets you set a keyboard button for an action using the button name. `action` is the action you wan to manage. `button_name` is a string containing th name of the button. `modifier_name` is a string containing the modifier for the command (default: ""). `control_index` is the index in the array of the action buttons you want to set (default: 0). If the array size is smaller than the `control_index`, the script adds a new element by itself.
- `ControlMap.set_joypad_button_name(action, button_name, modifier_name = "", control_index = 0)`:
This method lets you set a joypad button for an action using the button name. `action` is the action you wan to manage. `button_name` is a string containing the name of the button. `modifier_name` is a string containing the modifier for the command (default: ""). `control_index` is the index in the array of the action buttons you want to set (default: 0). If the array size is smaller than the `control_index`, the script adds a new element by itself.
- `ControlMap.set_keyboard_button_code(action, button_code, modifier_code = -1, control_index = 0)`:
This method lets you set a keyboard button for an action using the button code. `action` is the action you wan to manage. `button_code` is the key scancode. `modifier_code` is the modifier scancode (default: -1). `control_index` is the index in the array of the action buttons you want to set (default: 0). If the array size is smaller than the `control_index`, the script adds a new element by itself.
- `ControlMap.set_joypad_button_code(action, button_code, modifier_code = -1, control_index = 0)`:
This method lets you set a joypad button for an action using the button code. `action` is the action you wan to manage. `button_code` is the button index. `modifier_code` is the modifier scancode (default: -1). `control_index` is the index in the array of the action buttons you want to set (default: 0). If the array size is smaller than the `control_index`, the script adds a new element by itsel.
- `ControlMap.has_action(action)`:
This method returns true if the given `action` (a String) is in one of the input maps.
- `ControlMap.get_keyboard_buttons(action)`:
This method returns an Array with the mapped keyboard buttons names of an action.
- `ControlMap.get_joypad_buttons(action)`:
This method returns an Array with the mapped joypad buttons names of an action.
- `ControlMap.get_keyboard_button_as_string(action, command_index = 0)`:
This method returns a String containing the name of the button in the KeyBoardMap.get(action)[command_index] array. If there's a modifier attached and the `with_modifier` argument is true (as default) the string will be of the type "Modifier+Command".
- `ControlMap.get_joy_button_as_string(action, with_modifier = true, command_index = 0)`:
This method returns a String containing the name of the button in the JoypadMap.get(action)[command_index] array. If there's a modifier attached and the `with_modifier` argument is true (as default) the string will be of the type "Modifier+Command".
- `ControlMap.get_keyboard_modifier_as_string(action, command_index = 0`:
This method returns a String containing the name of the modifier associated with the KeyBoardMap.get(action)[command_index] command. If there's no modifier, it returns an empty String.
- `ControlMap.get_joy_modifier_as_string(action, command_index = 0`:
This method returns a String containing the name of the modifier associated with the JoypadMap.get(action)[command_index] command. If there's no modifier, it returns an empty String.
- `ControlMap.set_mouse_standard_input(enable)`:
This method allows the programmer to change the value of an internal boolean variable called `mouse_input_enabled`. You can use it in your code to enable and disable click input from the mouse.
Especially useful for GUI programming.


The script also comes with additional useful methods:
- `ControlMap.get_action_button_indexes(action)`:
This method returns an Array with the mapped joypad buttons indexes of an action.
- `ControlMap.get_action_key_scancodes(action)`:
This method returns an Array with the mapped keyboard buttons scancodes of an action.
- `ControlMap.get_event_button_name(event)`:
Given an `InputEvent` (like the one you get from `_input(event)` method), it can tell you the name of the button it corresponds to.
- `ControlMap.get_action(event)`:
Given an `InputEvent` (like the one you get from `_input(event)` method), it can tell you what action it corresponds to.
- `ControlMap.is_key_released(key_name)`:
Given the String representing the name of a button on the keyboard or the mouse, returns true if it has been released.
- `ControlMap.is_joy_button_released(joy_button_name)`:
Given the String representing the name of a button on the joypad, returns true if it has been released.
- `ControlMap.set_mouse_standard_input(enabled)`:
This method allows you to enable or disable the standard GUI input of the mouse. Unfortunately at the current state of the plugin, the only thing that changes is the value of the `mouse_standard_input_enabled` variable, as there is no other way to disable the mouse input globally on Godot 3.1. You can then use the set variable in your code to make Control nodes stop the mouse input. `enabled` is a boolean.
- `ControlMap.is_mouse_standard_input_enabled()`:
This method returns the value of the `mouse_standard_input_enabled` boolean.
- `ControlMap.save()`:
Allows the programmer to save the data stored in the input maps in a file. This method returns a `Dictionary` variables that can be stored at any time. Take a look at the official Godot Engine documentation if you want to know how to save your game data.
- `ControlMap.load_data(save_file)`:
Allows the programmer to load data from a file and store them in the input maps. This method returns `void`. The `save_file` argument is of type `File` and it has to be the file in wich the data are stored. Take a look at the official Godot Engine documentation if you want to know how to load your game data.


## IMPORTANT NOTES:
-  To use this plugin you have to add the ControlMap.gd script as an AutoLoad in order to make it work. You don't need to add the ConstantDictionaries.gd script or the InputMapTemplate.gd.
- Don't separate the scripts in the internal_scripts folder in two separate locations or the script won't work. However, putting the InputMapTemplate anywhere inside your project directory should be fine.
- If you want to use this script for the GUI input, you should clear the `Input Map` on your `ProjectSettings` to use this script. If you don't, weird things could happen with Control nodes input. Create your `InputMap` using the `Dictionary` variables given with this script. However this unfortunately means you will have to hard code every action on your scripts.
- At the current state (Version 0.9.8) this scipt has not received enough testing on the JoyPad front to make sure everything works correctly, especially with the Analog Sticks input. However, the Keyboard and Mouse front has been tested and appears to work as intended.
- Some of the buttons and keys in the ConstantDictionaries corresponds to more than one name string. That is due to help programmers, as I added all the way I thought one could call a button. In any case, you can add your own by duplicating a row and changing the name, or you can just change the name in the row itself. Be sure to add a comma at the end of the line if it`s not the last one.


If you have issues or concerns about this script you can contact me by opening an issue ticket on my [GitLab](https://gitlab.com/xecestel).

# Licenses

New Input Map Plugin
Copyright (C) 2019  Celeste Privitera

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at [https://mozilla.org/MPL/2.0/](https://mozilla.org/MPL/2.0/).

You can find more informations in the LICENSE.txt file.


# Changelog

### Version 0.9

- Script completed.
- Joypad input not tested yet.

### Version 0.9.5

- Added new feature: now you can have a modifier to your action command (i.e. "Shift+Spacebar").
- Edited saving method code: now there's a skeleton code to use it with a save script.
- Added new getters to work better with the new modifier feature (see above).

### Version 0.9.6

- Improved mouse input support.
- Added feature to enable and disable standard mouse input.
- Separated the script in two different scripts for better code reading.

### Version 0.9.8

- Moved the Configuration section to a separate script for better experience.
- Generalized save and load data feature.
- General improvements in code readability.

### Version 0.9.8.1

- Added new methods
- Fixed "internal_scripts" folder name
