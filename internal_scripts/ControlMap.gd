extends NewInputMap

####################################################################
#		NEW INPUT MAP FOR GODOT 3.1
#			Version 0.9
#			© Xecestel
####################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
#####################################
"""METHODS AND INTERNAL VARIABLES"""
#####################################

#variables#
onready var rel_path = self.get_script().get_path().get_base_dir();
onready var ConstantDictionaries = load(rel_path + "/ConstantDictionaries.gd").new()
var last_action : String;
###########

func _ready():
	self.pause_mode = Node.PAUSE_MODE_PROCESS;

#Using _process function checks when the last_action has been released
func _input(event):
	if (event is InputEventMouseMotion):
		return;
		
	if (self.is_action_just_released(last_action)):
		last_action = "";


############################
"""INPUT METHODS OVERRIDE"""
############################

#Overrides Input.is_action_pressed
func is_action_pressed(action : String) -> bool:
	if (self.has_action(action) == false):
		return false;
		
	var joybutton_indexes = self.get_action_button_indexes(action);
	var key_scancodes = self.get_action_key_scancodes(action);
	
	return self.check_joypad_input(joybutton_indexes) || self.check_keyboard_input(key_scancodes);

#Overrides Input.is_action_just_pressed method
func is_action_just_pressed(action : String) -> bool:
	if (!self.can_just_press(action)):
		return false;
		
	if (self.has_action(action) == false):
		return false;
		
	var joybutton_indexes = self.get_action_button_indexes(action);
	var key_scancodes = self.get_action_key_scancodes(action);
	var pressed = self.check_joypad_input(joybutton_indexes) || self.check_keyboard_input(key_scancodes);
		
	if (pressed):
		last_action = action;
		return true;
				
	return false;
	
#Overrides event.is_action_pressed method
func is_event_pressed(action : String, event : InputEvent) -> bool:	
	if (self.has_action(action) == false || event == null):
		return false;
		
	var joybutton_indexes = self.get_action_button_indexes(action);
	var key_scancodes = self.get_action_key_scancodes(action);
	var modifier_pressed = true;
	var event_pressed = false;
	var control_index = 0;
	
	if (event is InputEventJoypadButton):
		for index in joybutton_indexes:
			if (index.size() > 1):
				modifier_pressed = Input.is_joy_button_pressed(0, index[control_index]);
				control_index += 1;
			if (index[control_index] == event.button_index):
				event_pressed = true;
	elif (event is InputEventKey):
		for code in key_scancodes:
			if (code.size() > 1):
				modifier_pressed = Input.is_key_pressed(code[control_index]) if ConstantDictionaries.KEYBOARD_BUTTONS.values().has(code[control_index]) else Input.is_mouse_button_pressed(code[control_index]);
				control_index += 1;
			if (code[control_index] == event.scancode):
				event_pressed = true;
	elif (event is InputEventMouseButton):
		for code in key_scancodes:
			if (code.size() > 1):
				modifier_pressed = Input.is_key_pressed(code[control_index]) if ConstantDictionaries.KEYBOARD_BUTTONS.values().has(code[control_index]) else Input.is_mouse_button_pressed(code[control_index]);
				control_index += 1;
			if (code[control_index] == event.button_index):
				event_pressed = true;
	else:
		return false;
		
	return modifier_pressed && event_pressed;
#end
	
	
#Replaces event.is_action_pressed method with a "just_pressed" logic added
func is_event_just_pressed(action : String, event : InputEvent) -> bool:
	if (self.can_just_press(action) == false || event == null):
		return false;
	
	if (self.has_action(action) == false):
		return false;
		
	var joybutton_indexes = self.get_action_button_indexes(action);
	var key_scancodes = self.get_action_key_scancodes(action);
	var modifier_pressed = true;
	var event_pressed = false;
	var control_index = 0;
	
	if (event is InputEventJoypadButton):
		for index in joybutton_indexes:
			if (index.size() > 1):
				modifier_pressed = Input.is_joy_button_pressed(0, index[control_index]);
				control_index += 1;
			if (index[control_index] == event.button_index):
				last_action = action;
				event_pressed = true;
	elif (event is InputEventKey):
		for code in key_scancodes:
			if (code.size() > 1):
				modifier_pressed = Input.is_key_pressed(code[control_index]) if ConstantDictionaries.KEYBOARD_BUTTONS.values().has(code[control_index]) else Input.is_mouse_button_pressed(code[control_index]);
				control_index += 1;
			if (code[control_index] == event.scancode):
				last_action = action;
				event_pressed = true;
	elif (event is InputEventMouseButton):
		for code in key_scancodes:
			if (code.size() > 1):
				modifier_pressed = Input.is_key_pressed(code[control_index]) if ConstantDictionaries.KEYBOARD_BUTTONS.values().has(code[control_index]) else Input.is_mouse_button_pressed(code[control_index]);
				control_index += 1;
			if (code[control_index] == event.button_index):
				last_action = action;
				event_pressed = true;
	else:
		return false;
	
	return modifier_pressed && event_pressed;
	

#Overrides Input.is_action_just_released
func is_action_just_released(action : String) -> bool:
	if (self.has_action(action) == false):
		return false;
	
	var joybutton_indexes = self.get_action_button_indexes(action);
	var key_scancodes = self.get_action_key_scancodes(action);
	
	return !self.check_joypad_input(joybutton_indexes) && !self.check_keyboard_input(key_scancodes);
	

func is_key_released(key_name : String) -> bool:
	var key_code : int;
	
	if (ConstantDictionaries.KEYBOARD_BUTTONS.keys().has(key_name)):
		key_code = ConstantDictionaries.KEYBOARD_BUTTONS.get(key_name);
		return !Input.is_key_pressed(key_code);
	if (ConstantDictionaries.MOUSE_BUTTONS.keys().has(key_name)):
		key_code = ConstantDictionaries.MOUSE_BUTTONS.get(key_name);
		return !Input.is_mouse_button_pressed(key_code);
	
	return false;
	

	
func is_joy_button_released(joy_button_name : String) -> bool:
	var button_index : int;
	
	if (ConstantDictionaries.JOY_BUTTONS.keys().has(button_index)):
		button_index = ConstantDictionaries.JOY_BUTTONS.get(joy_button_name);
		return !Input.is_joy_button_pressed(0, button_index);
	
	return false;
	
#######################
"""INTERNAL METHODS"""
#######################

#Used to manage "just pressed" logic
func can_just_press(action : String) -> bool:
	return (action != last_action);
	
	
#Checks if the action exists
func has_action(action : String) -> bool:
	if (action == ""):
		print_debug("Invalid action name");
		return false;
		
	return KeyBoardMap.has(action) || JoypadMap.has(action);

#checks if the joypad input has been pressed
func check_joypad_input(codes : Array) -> bool:
	var command_index = 0;
	var modifier_pressed = true;
	
	for index in codes:
		if (index.size() > 1):
			modifier_pressed = Input.is_joy_button_pressed(0, index[command_index]);
			command_index += 1;
		if (Input.is_joy_button_pressed(0, index[command_index]) && modifier_pressed):
			return true;
			
	return false;

#Checks if the keyboard input has been pressed
func check_keyboard_input(codes : Array) -> bool:
	var modifier_pressed = true;
	var command_index = 0;
	
	for code in codes:
		if (code.size() > 1):
			modifier_pressed = Input.is_key_pressed(code[command_index]) if ConstantDictionaries.KEYBOARD_BUTTONS.values().has(code[command_index]) else Input.is_mouse_button_pressed(code[command_index]);
			command_index += 1;
		if (Input.is_key_pressed(code[command_index]) && modifier_pressed):
			return true;
	
	return false;

#########################
"""SETTERS AND GETTERS"""
#########################

#Allows to set a button to a given action using the button name
func set_keyboard_button_name(action : String, button_name : String, modifier_name : String = "", control_index : int = 0) -> bool:
	if (KeyBoardMap.has(action) == false):
		print_debug("Invalid action");
		return false;
	
	var new_command = [];
	
	if (modifier_name != ""):
		if (ConstantDictionaries.KEYBOARD_BUTTONS.has(modifier_name) || ConstantDictionaries.MOUSE_BUTTONS.has(modifier_name)):
			new_command.append(modifier_name);
		else:
			print_debug("Invalid modifier name");
			return false;

	if (ConstantDictionaries.KEYBOARD_BUTTONS.has(button_name) || ConstantDictionaries.MOUSE_BUTTONS.has(button_name)):
		new_command.append(button_name);
	else:
		print_debug("Invalid button name");
		return false;
	
	var action_index = KeyBoardMap.keys().find(action);
	return self.add_on_dictionary(KeyBoardMap, action_index, new_command, control_index);
#end
	
	
#Allows to set a button to a given action using the button name
func set_joypad_button_name(action : String, button_name : String, modifier_name : String = "", control_index : int = 0) -> bool:
	if (JoypadMap.has(action) == false):
		print_debug("Invalid action");
		return false;
		
	var new_command = [];
	
	if (modifier_name != ""):
		if (ConstantDictionaries.JOY_BUTTONS.has(modifier_name) || ConstantDictionaries.JOY_AXIS.has(modifier_name)):
			new_command.append(modifier_name);
		else:
			print_debug("Invalid modifier name");
			return false;

	if (ConstantDictionaries.JOY_BUTTONS.has(button_name) || ConstantDictionaries.JOY_AXIS.has(button_name)):
		new_command.append(button_name);
	else:
		print_debug("Invalid button name");
		return false;
	
	var action_index = KeyBoardMap.keys().find(action);
	return self.add_on_dictionary(KeyBoardMap, action_index, new_command, control_index);
#end
	
	
func add_on_dictionary(dict : Dictionary, action_id, new_command : Array, control_index : int = 0) -> bool:
	if (dict.values()[action_id].size() <= control_index):
		dict.values()[action_id].append(new_command);
	else:
		dict.values()[action_id][control_index] = new_command;
	return true;
	
	
#Allows to set a button to a given action using the button scancode
func set_keyboard_button_code(action : String, button_code : int, modifier_code : int = -1, control_index : int = 0) -> bool:
	if (KeyBoardMap.has(action) == false):
		print_debug("Invalid action");
		return false;
		
	var modifier_name : String = "";
	var button_name : String;
	
	if (modifier_code >= 0):
		if (ConstantDictionaries.KEYBOARD_BUTTONS.values().has(modifier_code)):
			var modifier_index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(modifier_code);
			modifier_name = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[modifier_index];
		elif (ConstantDictionaries.MOUSE_BUTTONS.values().has(button_code)):
			var modifier_index = ConstantDictionaries.MOUSE_BUTTONS.values().find(modifier_code);
			modifier_name = ConstantDictionaries.MOUSE_BUTTONS.keys()[modifier_index];
		else:
			print_debug("Invalid modifier code");
			return false;
	
	if (ConstantDictionaries.KEYBOARD_BUTTONS.values().has(button_code)):
		var button_index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[button_index];
	elif (ConstantDictionaries.MOUSE_BUTTONS.values().has(button_code)):
		var button_index = ConstantDictionaries.MOUSE_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.MOUSE_BUTTONS.keys()[button_index];
	else:
		print_debug("Invalid button code");
		return false;
	
	return self.set_keyboard_button_name(action, button_name, modifier_name, control_index);
#end
	
	
#Allows to set a button to a given action using the button index
func set_joypad_button_code(action : String, button_code : int, modifier_code : int = -1, control_index : int = 0) -> bool:
	if (JoypadMap.has(action) == false):
		print_debug("Invalid action");
		return false;
		
	var modifier_name : String = "";
	var button_name : String;
	
	if (modifier_code >= 0):
		if (ConstantDictionaries.JOY_BUTTONS.values().has(modifier_code)):
			var modifier_index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(modifier_code);
			modifier_name = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[modifier_index];
		elif (ConstantDictionaries.JOY_AXIS.values().has(button_code)):
			var modifier_index = ConstantDictionaries.MOUSE_BUTTONS.values().find(modifier_code);
			modifier_name = ConstantDictionaries.MOUSE_BUTTONS.keys()[modifier_index];
		else:
			print_debug("Invalid modifier code");
			return false;
	
	if (ConstantDictionaries.JOY_BUTTONS.values().has(button_code)):
		var button_index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[button_index];
	elif (ConstantDictionaries.JOY_AXIS.values().has(button_code)):
		var button_index = ConstantDictionaries.MOUSE_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.MOUSE_BUTTONS.keys()[button_index];
	else:
		print_debug("Invalid button code");
		return false;
	
	return self.set_joypad_button_name(action, button_name, modifier_name, control_index);
#end
	
	
#Allows to decide if the standard mouse button input should be registerd or not
func set_mouse_standard_input(enable : bool) -> void:
	self.mouse_input_enabled = enable;
#end
	
#Getter for the mouse_standard_input_enabled variable
func is_mouse_standard_input_enabled() -> bool:
	return self.mouse_input_enabled;
#end
	
	
#Given an action, returns the keyboard button names as an array of strings
func get_keyboard_buttons(action : String) -> Array:
	return KeyBoardMap.get(action);


#Given an action, returns the joypad button names as an array of strings
func get_joypad_buttons(action : String) -> Array:
	return JoypadMap.get(action);
	
	
func get_joy_button_as_string(action : String, with_modifier : bool = true, command_index : int = 0) -> String:
	var mapped_command : Array = JoypadMap.get(action)[command_index];
	var button_as_string : String = "";
	
	button_as_string += mapped_command[0];
	
	if (mapped_command.size() > 1):
		if (with_modifier):
			button_as_string += "+" + mapped_command[1];
		else:
			button_as_string = mapped_command[1];
	
	return button_as_string;
	

func get_joy_modifier_as_string(action : String, command_index : int = 0) -> String:
	var mapped_command : Array = JoypadMap.get(action)[command_index];
	var modifier_as_string : String = "";
	
	modifier_as_string += mapped_command[0];
	
	if (mapped_command.size() == 1):
		print_debug("No modifier attached");
		return "";
	
	return modifier_as_string;


func get_keyboard_modifier_as_string(action : String, command_index : int = 0) -> String:
	var mapped_command : Array = KeyBoardMap.get(action)[command_index];
	var modifier_as_string : String = "";
	
	modifier_as_string += mapped_command[0];
	
	if (mapped_command.size() == 1):
		print_debug("No modifier attached");
		return "";
	
	return modifier_as_string;
	
	
func get_keyboard_button_as_string(action : String, with_modifier : bool = true, command_index : int = 0) -> String:
	var mapped_command : Array = KeyBoardMap.get(action)[command_index];
	var button_as_string : String = "";
	
	button_as_string += mapped_command[0];
	
	if (mapped_command.size() > 1):
		if (with_modifier):
			button_as_string += "+" + mapped_command[1];
		else:
			button_as_string = mapped_command[1];
	
	return button_as_string;


#Given an action, returns the button indexes of the buttons
func get_action_button_indexes(action : String) -> Array:
	var joypad_commands_array = [];
	var joypad_mapped_commands = self.get_joypad_buttons(action);
	
	for command in joypad_mapped_commands:
		var controls = [];
		controls.append(ConstantDictionaries.JOY_BUTTONS.get(command[0])) if (ConstantDictionaries.JOY_BUTTONS.has(command[0])) else controls.append(ConstantDictionaries.JOY_AXIS.get(command[0]));
		if (command.size() > 1):
			controls.append(ConstantDictionaries.JOY_BUTTONS.get(command[1])) if (ConstantDictionaries.JOY_BUTTONS.has(command[1])) else controls.append(ConstantDictionaries.JOY_AXIS.get(command[1]));
		joypad_commands_array.append(controls);
	
	return joypad_commands_array;


#Given an action, returns the scancodes of the buttons
func get_action_key_scancodes(action : String) -> Array:
	var keyboard_commands_array = [];
	var keyboard_mapped_commands = self.get_keyboard_buttons(action);
	
	for command in keyboard_mapped_commands:
		var controls = [];
		controls.append(ConstantDictionaries.KEYBOARD_BUTTONS.get(command[0])) if (ConstantDictionaries.KEYBOARD_BUTTONS.has(command[0])) else controls.append(ConstantDictionaries.MOUSE_BUTTONS.get(command[0]));
		if (command.size() > 1):
			controls.append(ConstantDictionaries.KEYBOARD_BUTTONS.get(command[1])) if (ConstantDictionaries.KEYBOARD_BUTTONS.has(command[1])) else controls.append(ConstantDictionaries.MOUSE_BUTTONS.get(command[1]));
		keyboard_commands_array.append(controls);
	
	return keyboard_commands_array;

#Given an InputEvent, returns the button name
func get_event_button_name(event : InputEvent) -> String:
	var button_name;
	
	if (event is InputEventMouseButton):
		var button_code = event.button_index;
		var button_index = ConstantDictionaries.MOUSE_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.MOUSE_BUTTONS.keys()[button_index];
	elif (event is InputEventKey):
		var button_code = event.scancode;
		var button_index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[button_index];
	elif (event is InputEventJoypadButton):
		var button_code = event.button_index;
		var button_index = ConstantDictionaries.JOY_BUTTONS.values().find(button_code);
		button_name = ConstantDictionaries.JOY_BUTTONS.keys()[button_index];
	elif (event is InputEventJoypadMotion):
		var button_code = event.axis;
		var button_index = ConstantDictionaries.JOY_AXIS.values().find(button_code);
		button_name = ConstantDictionaries.JOY_AXIS.keys()[button_index]

	return button_name;


#Given an InputEvent, returns the action if it exists
func get_event_action(event : InputEvent) -> String:
	if (event is InputEventMouseMotion):
		return "";
		
	var action;
	
	if (event is InputEventKey):
		var index = ConstantDictionaries.KEYBOARD_BUTTONS.values().find(event.scancode);
		var keyboard_button = ConstantDictionaries.KEYBOARD_BUTTONS.keys()[index];
		if (!KeyBoardMap.values().has(keyboard_button)):
			return "";
		index = KeyBoardMap.values().find(keyboard_button);
		action = KeyBoardMap.keys()[index];
	if (event is InputEventJoypadButton):
		var index = ConstantDictionaries.JOY_BUTTONS.values().find(event.button_index);
		var keyboard_button = ConstantDictionaries.JOY_BUTTONS.keys()[index];
		if (!JoypadMap.values().has(keyboard_button)):
			return "";
		index = JoypadMap.values().find(keyboard_button);
		action = JoypadMap.keys()[index];
	if (event is InputEventMouseButton):
		var index = ConstantDictionaries.MOUSE_BUTTONS.values().find(event.button_index);
		var keyboard_button = ConstantDictionaries.MOUSE_BUTTONS.keys()[index];
		if (!KeyBoardMap.values().has(keyboard_button)):
			return "";
		index = KeyBoardMap.values().find(keyboard_button);
		action = KeyBoardMap.keys()[index];
	if (event is InputEventJoypadMotion):
		var index = ConstantDictionaries.JOY_AXIS.values().find(event.axis);
		var keyboard_button = ConstantDictionaries.JOY_AXIS.keys()[index];
		if (!JoypadMap.values().has(keyboard_button)):
			return "";
		index = JoypadMap.values().find(keyboard_button);
		action = JoypadMap.keys()[index];
		
	return action;
#end


#This method allows you to save the data stored in this script.
func save():
	var save_dict = {
		"JoypadMap"				:	JoypadMap,
		"KeyBoardMap"			:	KeyBoardMap,
		"mouse_input_enabled"	:	mouse_input_enabled
		}
	return save_dict;
#end

#This method allows you to load the data and store it in the script.
func load_data(save_file : File) -> bool:
	if (save_file == null):
		return false;
	
	var current_line = parse_json(save_file.get_line());
	
	for i in current_line.keys():
		if (i == "filename"):
			continue
		self.set(i, current_line[i]);
		
	return true;


###################
"""END OF SCRIPT"""
###################