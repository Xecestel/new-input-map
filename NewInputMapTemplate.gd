class_name NewInputMap
extends Node

####################################################################
#TEMPLATE FOR THE NEW INPUT MAP FOR GODOT 3.1
#			© Xecestel
####################################################################
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
############################################
"""CONFIGURATION"""
############################################
#This variable gives you more control over the mouse input
#Especially useful when programming GUIs
var mouse_input_enabled = true;

#Input map for the joypad
var JoypadMap = {
	"ui_accept"	: [ [ "XBOX_A" ] ],
	"ui_select"	: [ [ "XBOX_Y" ] ],
	"ui_cancel"	: [ [ "XBOX_B" ] ],
	"ui_left"	: [ [ "D-Pad Left" ] ],
	"ui_right"	: [ [ "D-Pad Right" ] ],
	"ui_up"	: [ [ "D-Pad Up" ] ],
	"ui_down"	: [ [ "D-Pad Down" ] ],
	}

#Input map for keyboard and mouse
var KeyBoardMap = {
	"ui_accept"	: [ [ "Enter" ], [ "Enter (KeyPad)" ], [ "Spacebar" ] ],
	"ui_select"	: [ [ "Spacebar" ], [ "" ] ],
	"ui_cancel"	: [ [ "Esc" ] ],
	"ui_focus_next" : [ [ "Tab" ] ],
	"ui_focus_prev" : [ [ "Shift", "Tab" ] ],
	"ui_left"	: [ [ "Left" ] ],
	"ui_right"	: [ [ "Right" ] ],
	"ui_up"	: [ [ "Up" ] ],
	"ui_down"	: [ [ "Down" ] ],
	"ui_page_up"	: [ [ "PageUp" ] ],
	"ui_page_down"	: [ [ "PageDown" ] ],
	"ui_home"	: [ [ "Home" ] ],
	"ui_end"	: [ [ "End" ] ]
	}
